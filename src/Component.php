<?php

declare(strict_types = 1);

namespace esnerda\Json2CsvProcessor;

use Keboola\Component\BaseComponent;

class Component extends BaseComponent {

    public function run(): void {
        $type = 'root';
        if ($this->getConfig()->getRootNode() != NULL) {
            $nodes = explode('.', $this->getConfig()->getRootNode());
            $type = $nodes[count($nodes) - 1];
        }
        $jsonParser = new JsonToCSvParser($this->getConfig()->getMapping(), $this->getLogger(), $type);

        $processor = new Processor(
                $jsonParser, $this->getConfig()->getAppendRowNr(),
                $this->getConfig()->isIncremental(), 
                $this->getConfig()->getRootNode(),
                $this->getConfig()->addFileName(),
                $this->getLogger()
        );



        $processor->convert($this->getDataDir(), $this->getConfig()->getInputType());
    }

    protected function getConfigClass(): string {
        return Config::class;
    }

    protected function getConfigDefinitionClass(): string {
        return ConfigDefinition::class;
    }

}
